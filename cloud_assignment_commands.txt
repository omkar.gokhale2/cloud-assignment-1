Pig (for cleaning the data)
1-To Load Data into Pig keeping csv extension :- 
data = load '/Assignment1/political-emails.csv' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE', 'UNIX', 'SKIP_INPUT_HEADER') as ( ID:int, sender_name:chararray, email:chararray,subject:chararray, datetime:Datetime, cleaned_content:chararray);
2-For removing extra spaces and characters from the data : -
clean_data = FOREACH data GENERATE ID,sender_name,email, REPLACE(REPLACE(REPLACE(cleaned_content,'\\s+',' '),'([^a-zA-Z0-9\\s]+)',''),'\\s+',' ') as (cleaned_content:chararray);
3-For removing blank/NA values of the email id :- 
clean_data_noblank = filter clean_data by not (email=='');
4-For storing the pig output into the hdfs cluster :-
store clean_data_noblank into 'hdfs://cluster-9f73-m/Assignment1/Pigresult' using org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE', 'UNIX', 'SKIP_INPUT_HEADER');


Hive (for separating Spam and Ham and getting top 10 of each)
Database created for Cloud Assignment :- Use assignment1;
1- Creating external table for Pig Data :- 
create external table if not exists PigData (ID int,sender string, email string, content string) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
2- To load the Pig data into table :-
load data inpath 'hdfs://cluster-9f73-m/Assignment1/Pigresult' into table PigData;
3- Creating external table for BOW :-
create external table if not exists spamdata (spam string) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
4- To load spam BOW into hive table :-
LOAD DATA INPATH 'hdfs://cluster-9f73-m/Assignment1/Bag_of_Words.csv' INTO TABLE SpamData;
5-Seperating SPAM :-
create table spammails as select * from pigdatafinal as pd where exists (select * from spamdata where pd.content like concat('%',spam,'%'));
6- Creating table for BOW for HAM :-
create external table if not exists hamwords (ham string) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
7- Loading HAM BOW :-
LOAD DATA INPATH 'hdfs://cluster-9f73-m/Assignment1/Ham_words.txt' INTO TABLE hamwords;
8- Seperating HAM :-
create table hammail as select * from pigdatafinal as pd where exists (select * from hamwords where pd.content like concat('%',ham,'%'));
9- Getting top 10 spam mailers :-
create table topspam as select email, count(email) from spammails group by email order by count(email) desc limit 10;
10- Getting top 10 ham mailers :-
create table topham as select email, count(email) from hammail group by email order by count(email) desc limit 10;


TFIDF Hive SPAM
1- Creating table by Joining tables to get content of top10 spam mailers :-
create table topinfospam as select sm.id,sm.sender,sm.email,sm.content from spammails sm join topspam ts on (sm.email = ts.email);

2- Creating table for TFIDF calculation :-
create table tfidftopspam as select id,email,sender,content,length(content) as textlen from topinfospam where length(content) in (select max(length(content)) from topinfospam group by email order by max(length(content)) desc) order by length(content) desc;

3- Creating external table for exporting HIVE output for mapreduce :-
CREATE EXTERNAL TABLE tfidfexport(id int,email string,sender string,textlen string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
"separatorChar" = ",",
"quoteChar" = "\""
)
STORED AS TEXTFILE
LOCATION '/test';

4- Loading/inserting data into the export table :- 
insert into tfidfexport select id,email,content,textlen from assignment1.tfidftopspam;

5- Running Mapper and Reducer on Hive output file in HADOOP :-
hadoop jar /usr/lib/hadoop/hadoop-streaming.jar -file /home/omkar_gokhale2/map1.py /home/omkar_gokhale2/reduce1.py -mapper "python map1.py" -reducer "python reduce1.py" -input /test/000000_0 -output /mapred/spam/output1
hadoop jar /usr/lib/hadoop/hadoop-streaming.jar -file /home/omkar_gokhale2/map2.py /home/omkar_gokhale2/reduce2.py -mapper "python map2.py" -reducer "python reduce2.py" -input /mapred/spam/output1/part-0000* -output /mapred/spam/output2
hadoop jar /usr/lib/hadoop/hadoop-streaming.jar -file /home/omkar_gokhale2/map3.py /home/omkar_gokhale2/reduce3.py -mapper "python map3.py" -reducer "python reduce3.py" -input /mapred/spam/output2/part-0000* -output /mapred/spam/output3
hadoop jar /usr/lib/hadoop/hadoop-streaming.jar -files /home/omkar_gokhale2/map4.py -mapper "python map4.py" -input /mapred/spam/output3/part-0000* -output /mapred/spam/output4

6- Creating table for Mapreduce output :-
CREATE external TABLE IF not exists spamtfidfmr (word string, id string, tfidf double) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' WITH SERDEPROPERTIES ("separatorChar"="\t") LOCATION '/home/omkar_gokhale2/spam-tfidf-value.csv' tblproperties("skip.header.line.count"="0");

7- Loading output of Mapreduce into HIVE :-
Load data local inpath '/home/omkar_gokhale2/spam-tfidf-value.csv' overwrite into table spamtfidfmr;

8- Getting the top 10 spam mailers words and their TFIDF :-
select id,word,tfidf from spamtfidfmr where id in (select id from tfidftopspam order by textlen desc limit 10) and length(word)>4 order by id asc;

9- Getting Every 10 ID's top words and thier ranks :-  
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '1103' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '3760' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '47440' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '51444' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '52195' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '64623' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '65859' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '7072' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '7764' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '83575' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM spamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '84428' and rank < 10 ORDER BY id, rank;

tfidf Hive HAM
1- Creating table by Joining tables to get content of top10 ham mailers :-
create table topinfoham as select hm.id,hm.sender,hm.email,hm.content from hammail hm join topham th on (hm.email = th.email);

2- Creating table for TFIDF calculation :-
create table tfidftopham as select id,email,sender,content,length(content) as textlen from topinfoham where length(content) in (select max(length(content)) from topinfoham group by email order by max(length(content)) desc) order by length(content) desc;

3- Creating external table for exporting HIVE output for mapreduce :-
CREATE EXTERNAL TABLE tfidfhamexport(id int,email string,sender string,textlen string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
"separatorChar" = ",",
"quoteChar" = "\""
)
STORED AS TEXTFILE
LOCATION '/testham';

4- Loading/inserting data into the export table :-
insert into tfidfhamexport select id,email,content,textlen from assignment1.tfidftopham;

5- Running Mapper and Reducer on Hive output file in HADOOP :-
hadoop jar /usr/lib/hadoop/hadoop-streaming.jar -file /home/omkar_gokhale2/map1.py /home/omkar_gokhale2/reduce1.py -mapper "python map1.py" -reducer "python reduce1.py" -input /testham/000000_0 -output /mapred/ham/output1
hadoop jar /usr/lib/hadoop/hadoop-streaming.jar -file /home/omkar_gokhale2/map2.py /home/omkar_gokhale2/reduce2.py -mapper "python map2.py" -reducer "python reduce2.py" -input /mapred/ham/output1/part-0000* -output /mapred/ham/output2
hadoop jar /usr/lib/hadoop/hadoop-streaming.jar -file /home/omkar_gokhale2/map3.py /home/omkar_gokhale2/reduce3.py -mapper "python map3.py" -reducer "python reduce3.py" -input /mapred/ham/output2/part-0000* -output /mapred/ham/output3
hadoop jar /usr/lib/hadoop/hadoop-streaming.jar -files /home/omkar_gokhale2/map4.py -mapper "python map4.py" -input /mapred/ham/output3/part-0000* -output /mapred/ham/output4
hadoop fs -rm /mapred/ham/output4/_SUCCESS
hadoop fs -getmerge /mapred/ham/output4/ /home/omkar_gokhale2/ham-tfidf-value.csv

6- Creating table for Mapreduce output :-
CREATE external TABLE IF not exists hamtfidfmr (word string, id string, tfidf double) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' WITH SERDEPROPERTIES ("separatorChar"="\t") LOCATION '/home/omkar_gokhale2/ham-tfidf-value.csv' tblproperties("skip.header.line.count"="0");

7- Loading output of Mapreduce into HIVE :-
Load data local inpath '/home/omkar_gokhale2/ham-tfidf-value.csv' overwrite into table hamtfidfmr;

8- Getting the top 10 ham mailers words and their TFIDF :-
select id,word,tfidf from hamtfidfmr where id in (select id from tfidftopham order by textlen desc limit 10) and length(word)>4 order by id asc;

9- Getting Every 10 ID's top words and thier ranks :-
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '1103' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '23376' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '23484' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '2611' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '4153' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '43548' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '52195' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '64623' and rank < 10 ORDER BY id, rank;
SELECT * FROM (SELECT id,word,tfidf, rank() over (PARTITION BY id ORDER BY tfidf DESC) as rank FROM hamtfidfmr DISTRIBUTE BY id SORT BY id desc) ranks WHERE id = '7764' and rank < 10 ORDER BY id, rank;
